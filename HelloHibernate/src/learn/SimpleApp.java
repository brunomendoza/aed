package learn;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SimpleApp {
	
	public static void main(String[] args) {
		String protocol = "jdbc:derby";
		String framework = "emmbeded";
		String url = protocol + ":db/hello_hibernate;create=true";
		Connection conn = null;
		
		try {
			conn = DriverManager.getConnection(url);
			
			System.out.println("A db connection has been done.");
			
			conn.close();
			
			System.out.println("The db connection has been closed.");
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
	}
}
