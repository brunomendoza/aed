connect 'jdbc:derby:db/hello_hibernate;create=true';

CREATE TABLE albums (
  album_id INT GENERATED ALWAYS AS IDENTITY,
  title VARCHAR(25) NOT NULL,
  author VARCHAR(25) NOT NULL,
  CONSTRAINT pK_album_id PRIMARY KEY (album_id)
);

INSERT INTO albums (title, author) VALUES
('b1', 't1'),
('b2', 't2'),
('b3', 't3'),
('b5', 't5'),
('b4', 't4');
